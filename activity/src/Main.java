public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        Contact friend1 = new Contact("John Doe", "+639152468596", "Quezon City");
//        System.out.println(friend1.getName());
//        System.out.println(friend1.getContactNumber());
//        System.out.println(friend1.getAddress());
        Contact friend2 = new Contact();
//        friend2.setName("Jane Doe");
//        System.out.println(friend2.getName());
//        friend2.setContactNumber("+639162148573");
//        System.out.println(friend2.getContactNumber());
//        friend2.setAddress("Caloocan City");
//        System.out.println(friend2.getAddress());

        phonebook.addContact(friend1);
        phonebook.addContact(friend2);

        if (phonebook.getContacts().size() == 0) {
            System.out.println("Phonebook is empty.");
        } else {
            for (Contact contact : phonebook.getContacts()) {
                System.out.println(contact.getName());
                System.out.println("--------------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                System.out.println(contact.getContactNumber());
                System.out.println("--------------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                System.out.println(contact.getAddress());
                System.out.println("====================");
            }
        }

    }
}