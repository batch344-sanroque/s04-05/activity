public class Contact {

    private String name;
    private String contactNumber;
    private String address;
    private Phonebook contacts;

    public Contact(){
        this.name = "empty contact";
        this.contactNumber = "empty contact";
        this.address = "empty contact";
    }

    public Contact(String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
        this.contacts = new Phonebook(){};
    }

    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getContactNumber() {
        return this.contactNumber;
    }
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }
    public String getAddress() {
        return this.address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

}
