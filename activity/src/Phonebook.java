
import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts;

    public Phonebook(){
        this.contacts = new ArrayList<>();
    }

    public ArrayList<Contact> getContacts(){
        return contacts;
    }
    public void addContact(Contact contact){
        contacts.add(contact);
    }
}
